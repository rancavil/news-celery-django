from django.contrib import admin

from .models import Topic, Customer, New


class TopicAdmin(admin.ModelAdmin):
    list_display = ('topic','description')

class CustomerAdmin(admin.ModelAdmin):
    list_display = ('user', 'active')

class NewAdmin(admin.ModelAdmin):
    list_display = ('title', 'topic', 'text')

admin.site.register(Topic, TopicAdmin)
admin.site.register(Customer, CustomerAdmin)
admin.site.register(New, NewAdmin)
