import os
import smtplib, ssl
from email.message import EmailMessage
from datetime import datetime as dt

EMAIL_SERVER = os.environ['EMAIL_SERVER']
EMAIL_PORT = int(os.environ['EMAIL_PORT'])
EMAIL_ACCOUNT = os.environ['EMAIL_ACCOUNT']
EMAIL_PASSWD = os.environ['EMAIL_PASSWD']

def send_mail(email, subject, content):
    """
    """
    message = EmailMessage()
    message['Subject'] = subject
    message['From'] = EMAIL_ACCOUNT
    message['To'] = email
    message.set_content(content)

    with smtplib.SMTP_SSL(EMAIL_SERVER, EMAIL_PORT, context=ssl.create_default_context(), timeout=10) as server:
        server.login(EMAIL_ACCOUNT, EMAIL_PASSWD)
        server.send_message(message)
