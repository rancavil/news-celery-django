import os

import feedparser

RSS_URL = os.environ['RSS_URL']

def read_rss(topic):
    """
    """
    news_feed = feedparser.parse('{}?q={}'.format(RSS_URL,topic))

    return news_feed.entries
