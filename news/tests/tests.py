import pytest

from django.contrib.auth import get_user_model

from news.models import New, Topic
from news.tasks import get_news_feed


@pytest.mark.django_db(transaction=True)
@pytest.mark.parametrize('username', ['barney'])
def test_creating_news(
            celery_app, 
            celery_worker, 
            news_data):
    """
    """
    news = New.objects.create(**news_data)

    news_created = New.objects.all().count()
    assert  news_created == 1

@pytest.mark.django_db(transaction=True)
@pytest.mark.parametrize('username', ['barney'])
def test_task_news_feed(
            celery_app, 
            celery_worker, 
            create_test_topic):
    """
    """
    result = get_news_feed.delay(n_news=1)
    result.get()
    assert result.status == 'SUCCESS'

    news = New.objects.all()
    assert news.count() == 1
