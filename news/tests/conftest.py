import os

import pytest

from django.contrib.auth import get_user_model
from celery.contrib.pytest import *

from news.models import Topic, Customer


@pytest.fixture
def user_data(username):
    """
    """
    return {'email' : os.environ['EMAIL_ACCOUNT'],
            'username' : username, 
            'password' : 'xyz123abc'}

@pytest.fixture
def create_test_topic():
    """
    """
    topic = Topic.objects.create(
            topic='Technology', 
            description='News about Technology')

    return topic

@pytest.fixture(autouse=True)
def create_test_customer(user_data, create_test_topic):
    """
    """
    user_model = get_user_model()
    user = user_model.objects.create_user(**user_data)
    
    customer = Customer.objects.get(user=user)
    customer.topics.add(create_test_topic)

    return customer

@pytest.fixture
def news_data(create_test_topic):
    """
    """
    return {'topic' : create_test_topic, 
            'title' : 'pytest', 
            'text' : 'pytest and django to test your application'}

@pytest.fixture(scope='session')
def celery_config():
    """
    """
    return {'result_backend' : os.environ['REDIS'],
            'broker_url' : os.environ['RABBITMQ'],
            'accept_content' : ['json'],
            'task_serializer' : 'json',
            }
