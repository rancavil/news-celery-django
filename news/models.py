from django.db import models
from django.contrib.auth.models import User


class Topic(models.Model):
    """
    """
    topic = models.CharField(max_length=100, primary_key=True)
    description = models.TextField(blank=True, default=None)
    pub_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.topic

class Customer(models.Model):
    """
    """
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    active = models.BooleanField(default=True)
    topics = models.ManyToManyField(Topic) 

    def __str__(self):
        return self.user.email


class New(models.Model):
    """
    """
    topic = models.ForeignKey(Topic, on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now=True)
    title = models.CharField(max_length=100)
    text = models.TextField(blank=True, default=None)

    def __str__(self):
        return self.title

