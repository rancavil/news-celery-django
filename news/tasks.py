from celery import shared_task

from .models import Topic, New, Customer
from .api.mail import send_mail
from .api.rss import read_rss


@shared_task(bind=True)
def send_email(self, topic, id_news):
    """
    """
    subscribers = Topic.objects.get(topic=topic).customer_set.filter(active=True)
    news = New.objects.get(id=id_news)
    for subscriber in subscribers:
        email = subscriber.user.email
        subject = news.title
        content = news.text

        send_mail(email, subject, content)

@shared_task(bind=True)
def get_news_feed(self, n_news=10):
    """
    """
    for topic in Topic.objects.all():
        feeds = read_rss(topic.topic)
        for feed in feeds[0:n_news]:
            news = New.objects.create(
                topic=topic, 
                title=feed.title[0:50], 
                text=feed.links[0].href)
            news.save()
