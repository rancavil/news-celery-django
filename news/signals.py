from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver
from django.contrib.auth.models import User

from .models import New, Topic, Customer
from .tasks import send_email


@receiver(post_save, sender=New)
def inform_news(sender, instance, created, **kwargs):
    """
    """
    if created:
        send_email.delay(instance.topic.topic, instance.id)


@receiver(post_save, sender=User)
def create_user_customer(sender, instance, created, **kwargs):
    """
    """
    if created:
        customer = Customer.objects.create(user=instance)
        customer.save()
