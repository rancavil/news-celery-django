# news-celery-django

It is a Django application that uses Celery to execute async tasks.

## Software:

- Python 3.9.0
- Django 3.1.7 
- Celery 5.0.5
- Pytest 6.2.2
- PostgreSQL 9.6.1
- rabbitmq (broker) 3.6.10
- redis (backend) 6.2.1

## Description:

The application implements three Models:

**Customer** are users that subscribe to topics to receive news about them. Each user can be a topic's subscriber (one or more).

**Topic** are used to classify information. Each topic has associated customers (subscriber).

**News** are information classified by Topic, every to be delivered to a group of users (subscribers).

The application allows **customers** to subscribe to **topics** to receive **news**. The news is collected by a crontab daemon that retrieves news from an RSS source. Every time that news is retrieved, it is inserted in the News model, triggering a signal that sends an email to subscribers of that Topic.


## Configuration:

RSS_URL configures rss source.

     $ export RSS_URL=https://news.google.com/rss/search

EMAIL environment vars configure SMTP server.

    $ export EMAIL_SERVER=<smtp-server>
    $ export EMAIL_PORT=465
    $ export EMAIL_ACCOUNT=<your-source-email>
    $ export EMAIL_PASSWD=<your-email-password>
    $ export RABBITMQ=amqp://<username>:<password>@<address-rabbitmq-server>//
    $ export REDIS=redis://<address-redis-server>/1

ToDO: I am using docker-compose to get the enviroment.

## Test

The following command executes **pytest** (a runner is configured into setting.py)

    $ ./manage.py test

