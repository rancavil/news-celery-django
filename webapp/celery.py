import os

from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE','webapp.settings')

app = Celery(
        'news',
        backend=os.environ['REDIS'],
        broker=os.environ['RABBITMQ'],
        )

app.conf.update(
        accept_content=['json'],
        task_serializer='json',
        )

app.config_from_object('django.conf:settings',namespace='CELERY')
app.autodiscover_tasks()
